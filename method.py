
# curl -X 'POST' \
#   'https://test1.vlin.co.in/sdapi/v1/extra-single-image' \
#   -H 'accept: application/json' \
#   -H 'Content-Type: application/json' \
#   -d '{
#   "resize_mode": 0,
#   "show_extras_results": false,
#   "gfpgan_visibility": 1,
#   "codeformer_visibility": 0,
#   "codeformer_weight": 0,
#   "upscaling_resize": 2,
#   "upscaling_resize_w": 512,
#   "upscaling_resize_h": 512,
#   "upscaling_crop": true,
#   "upscaler_1": "ESRGAN_4x",
#   "upscaler_2": "None",
#   "extras_upscaler_2_visibility": 0,
#   "upscale_first": false,
#   "image": ""
# }'
import time
import numpy as np
import requests
import base64
from PIL import Image
from io import BytesIO

def base64_to_image(base64_string):
    # Chuyển đổi base64 string thành dữ liệu byte
    image_data = base64.b64decode(base64_string)
    
    # Tạo một stream từ dữ liệu byte
    image_stream = BytesIO(image_data)
    
    # Tạo một hình ảnh từ stream
    image = Image.open(image_stream)

    return image

def convert_image_to_base64(image):
    buffered = BytesIO()
    image.save(buffered, format="PNG")
    return base64.b64encode(buffered.getvalue()).decode()

def process_upscale(image, scale_number):
    # Chuyển đổi PIL Image sang base64
    image = Image.fromarray(image).convert("RGB")
    image_base64 = convert_image_to_base64(image)

    # Thiết lập URL và headers cho request
    url = 'https://test1.vlin.co.in/sdapi/v1/extra-single-image'
    headers = {
        'accept': 'application/json',
        'Content-Type': 'application/json'
    }

    # Tạo JSON body cho request
    data = {
        "resize_mode": 0,
        "show_extras_results": False,
        "gfpgan_visibility": 1,
        "codeformer_visibility": 0,
        "codeformer_weight": 0,
        "upscaling_resize": scale_number,
        "upscaling_resize_w": 512,
        "upscaling_resize_h": 512,
        "upscaling_crop": True,
        "upscaler_1": "None",
        "upscaler_2": "None",
        "extras_upscaler_2_visibility": 0,
        "upscale_first": False,
        "image": image_base64
    }

    # Gửi request POST
    t0 = time.time()
    response = requests.post(url, json=data, headers=headers)
    if response.status_code == 200:
        image_base64 = response.json()['image']
    t1 = time.time()
    info = f"Thời gian xử lý của server: {t1-t0} s"
    return np.array(base64_to_image(image_base64)), info

# Sử dụng function
# Ví dụ: image = Image.open('path_to_your_image.png')
# response = send_image_request(image)

# def process_upscale(image):
#     return image, "Thông tin"
    