import gradio as gr
import json
import pandas as pd
from utils import PaperCuttingSolution
from method import process_upscale

DATA_EQUIPMENT = json.load(open('data_equipment.json'))

data_df = []
for key, value in DATA_EQUIPMENT.items():
    name = key.replace(" ", "").lower()
    data_df.append(
        [name, value['height'], value['width'], value['num_of_color_default'], value['num_of_color_additional'], value['normal'], value['penalty']]) 


data = json.load(open('data_paper.json'))
# Load loại giấy
list_type_papers = []
# Mapping loại giấy với trọng lượng
mapping_paper_weight = {}
for key in list(data.keys()):
    _type_paper = key.split('#')[0].strip()
    if _type_paper not in list_type_papers:
        list_type_papers.append(_type_paper)
        if _type_paper not in mapping_paper_weight:
            mapping_paper_weight[_type_paper] = sorted(
                list(map(int, list(data[key].keys()))))

list_equipment_size = ["Auto", "360x520", "520x720", "790x1090", "Custom"]
paper_size_list = ["Auto", "650x860", "790x1090", "Custom"]

def save_data(data):
    global list_equipment_size
    global data_df
    data_df = data
    
    return data

def update_paper_weight(type_paper):
    return gr.update(
        choices=mapping_paper_weight.get(
            type_paper, []), value=None)


def update_custom_equipment_size(selected_size):
    if selected_size == "Custom":
        return gr.update(visible=True), gr.update(visible=True)
    else:
        return gr.update(visible=False), gr.update(visible=False)


def update_custom_paper_size(selected_size):
    if selected_size == "Custom":
        return gr.update(visible=True), gr.update(visible=True)
    else:
        return gr.update(visible=False), gr.update(visible=False)


def update_custom_num_of_slice(selected_num_of_slice):
    if selected_num_of_slice == 2:
        return gr.update(visible=True)
    else:
        return gr.update(visible=False)


solutions = {}

def calculate_area(w, h, G, D):
    
    ratio = 0.775 
    
    W = 2*w + 2*D + 10
    H = G + h + D + D*0.8
    
    area = ratio * (W*H)
    area = int(area)
    
    return f"{area} (mm^2)", f"{W} (mm)", f"{H} (mm)"

def calculate_solutions(
        piece_width,
        piece_height,
        piece_quantity,
        num_of_slice,
        flip_type,
        type_paper,
        paper_weight,
        equipment_size,
        custom_equipment_width,
        custom_equipment_height,
        paper_size,
        custom_paper_width,
        custom_paper_height,
        num_of_color,
        giacong_canmang_dongia,
        giacong_canmang_matphut,
        giacong_epkim_chieudai,
        giacong_epkim_chieurong,
        giacong_epkim_dongiakhuon,
        giacong_epkim_dongialuot,
        giacong_epkim_dongiatoithieu,
        giacong_cangap_soduongchay,
        giacong_cangap_dongia,
        giacong_cangap_giatoithieu,
        giacong_bekhuon_dongiakhuon,
        giacong_bekhuon_dongialuot):

    global solutions

    # Trường hợp giải quyết nhiều
    if paper_size == "Auto" and equipment_size == "Auto":
        solutions = PaperCuttingSolution.solve_multi(
            piece_size=(int(piece_width), int(piece_height)),
            number_of_print_slide=num_of_slice,
            flip_type=flip_type,
            paper_type=type_paper,
            paper_gsm=paper_weight,
            piece_quantity=piece_quantity,
            num_of_color=num_of_color,
            giacong_canmang_dongia=giacong_canmang_dongia,
            giacong_canmang_matphut=giacong_canmang_matphut,
            giacong_epkim_chieudai=giacong_epkim_chieudai,
            giacong_epkim_chieurong=giacong_epkim_chieurong,
            giacong_epkim_dongiakhuon=giacong_epkim_dongiakhuon,
            giacong_epkim_dongialuot=giacong_epkim_dongialuot,
            giacong_epkim_dongiatoithieu=giacong_epkim_dongiatoithieu,
            giacong_cangap_soduongchay=giacong_cangap_soduongchay,
            giacong_cangap_dongia=giacong_cangap_dongia,
            giacong_cangap_giatoithieu=giacong_cangap_giatoithieu,
            giacong_bekhuon_dongiakhuon=giacong_bekhuon_dongiakhuon,
            giacong_bekhuon_dongialuot=giacong_bekhuon_dongialuot
        )
        for key, value in solutions.items():
            solutions[key]['df'] = pd.DataFrame(value['df'])
        for key, value in solutions.items():
            return value['df'], value['data'], value['image'], value['image_2']

    # Trường hợp giải tự chọn
    if paper_size not in [
        "Auto",
        "Custom"] and equipment_size not in [
        "Auto",
            "Custom"]:
        solutions = PaperCuttingSolution.solve_single(
            piece_size=(int(piece_width), int(piece_height)),
            paper_size=(int(paper_size.split("x")[0]), int(paper_size.split("x")[1])),
            equipment_size=(int(equipment_size.split("x")[0]), int(equipment_size.split("x")[1])),
            number_of_print_slide=num_of_slice,
            flip_type=flip_type,
            paper_type=type_paper,
            paper_gsm=paper_weight,
            piece_quantity=piece_quantity,
            num_of_color=num_of_color,
            giacong_canmang_dongia=giacong_canmang_dongia,
            giacong_canmang_matphut=giacong_canmang_matphut,
            giacong_epkim_chieudai=giacong_epkim_chieudai,
            giacong_epkim_chieurong=giacong_epkim_chieurong,
            giacong_epkim_dongiakhuon=giacong_epkim_dongiakhuon,
            giacong_epkim_dongialuot=giacong_epkim_dongialuot,
            giacong_epkim_dongiatoithieu=giacong_epkim_dongiatoithieu,
            giacong_cangap_soduongchay=giacong_cangap_soduongchay,
            giacong_cangap_dongia=giacong_cangap_dongia,
            giacong_cangap_giatoithieu=giacong_cangap_giatoithieu,
            giacong_bekhuon_dongiakhuon=giacong_bekhuon_dongiakhuon,
            giacong_bekhuon_dongialuot=giacong_bekhuon_dongialuot
        )
        for key, value in solutions.items():
            solutions[key]['df'] = pd.DataFrame(value['df'])
        for key, value in solutions.items():
            return value['df'], value['data'], value['image'], value['image_2']

    if paper_size == "Custom" and equipment_size == "Custom":
        solutions = PaperCuttingSolution.solve_single(
            piece_size=(int(piece_width), int(piece_height)),
            paper_size=(custom_paper_width, custom_paper_height),
            equipment_size=(custom_equipment_width, custom_equipment_height),
            number_of_print_slide=num_of_slice,
            paper_type=type_paper,
            paper_gsm=paper_weight,
            piece_quantity=piece_quantity,
            num_of_color=num_of_color,
            giacong_canmang_dongia=giacong_canmang_dongia,
            giacong_canmang_matphut=giacong_canmang_matphut,
            giacong_epkim_chieudai=giacong_epkim_chieudai,
            giacong_epkim_chieurong=giacong_epkim_chieurong,
            giacong_epkim_dongiakhuon=giacong_epkim_dongiakhuon,
            giacong_epkim_dongialuot=giacong_epkim_dongialuot,
            giacong_epkim_dongiatoithieu=giacong_epkim_dongiatoithieu,
            giacong_cangap_soduongchay=giacong_cangap_soduongchay,
            giacong_cangap_dongia=giacong_cangap_dongia,
            
            giacong_cangap_giatoithieu=giacong_cangap_giatoithieu,
            giacong_bekhuon_dongiakhuon=giacong_bekhuon_dongiakhuon,
            giacong_bekhuon_dongialuot=giacong_bekhuon_dongialuot
        )
        for key, value in solutions.items():
            solutions[key]['df'] = pd.DataFrame(value['df'])
        for key, value in solutions.items():
            return value['df'], value['data'], value['image'], value['image_2']

    # paper size auto, euipment custom
    if paper_size == "Auto" and equipment_size == "Custom":
        solutions = PaperCuttingSolution.solve_paper_size_auto(
            equipment_size=(custom_equipment_width, custom_equipment_height),
            piece_size=(int(piece_width), int(piece_height)),
            number_of_print_slide=num_of_slice,
            flip_type=flip_type,
            paper_type=type_paper,
            paper_gsm=paper_weight,
            piece_quantity=piece_quantity,
            num_of_color=num_of_color,
            giacong_canmang_dongia=giacong_canmang_dongia,
            giacong_canmang_matphut=giacong_canmang_matphut,
            giacong_epkim_chieudai=giacong_epkim_chieudai,
            giacong_epkim_chieurong=giacong_epkim_chieurong,
            giacong_epkim_dongiakhuon=giacong_epkim_dongiakhuon,
            giacong_epkim_dongialuot=giacong_epkim_dongialuot,
            giacong_epkim_dongiatoithieu=giacong_epkim_dongiatoithieu,
            giacong_cangap_soduongchay=giacong_cangap_soduongchay,
            giacong_cangap_dongia=giacong_cangap_dongia,
            
            giacong_cangap_giatoithieu=giacong_cangap_giatoithieu,
            giacong_bekhuon_dongiakhuon=giacong_bekhuon_dongiakhuon,
            giacong_bekhuon_dongialuot=giacong_bekhuon_dongialuot
        )
        for key, value in solutions.items():
            solutions[key]['df'] = pd.DataFrame(value['df'])
        for key, value in solutions.items():
            return value['df'], value['data'], value['image'], value['image_2']

    if paper_size == "Auto" and equipment_size not in ["Custom", "Auto"]:
        equipment_size = list(map(int, equipment_size.split('x')))
        solutions = PaperCuttingSolution.solve_paper_size_auto(
            equipment_size=(equipment_size[0], equipment_size[1]),
            piece_size=(int(piece_width), int(piece_height)),
            number_of_print_slide=num_of_slice,
            flip_type=flip_type,
            paper_type=type_paper,
            paper_gsm=paper_weight,
            piece_quantity=piece_quantity,
            num_of_color=num_of_color,
            giacong_canmang_dongia=giacong_canmang_dongia,
            giacong_canmang_matphut=giacong_canmang_matphut,
            giacong_epkim_chieudai=giacong_epkim_chieudai,
            giacong_epkim_chieurong=giacong_epkim_chieurong,
            giacong_epkim_dongiakhuon=giacong_epkim_dongiakhuon,
            giacong_epkim_dongialuot=giacong_epkim_dongialuot,
            giacong_epkim_dongiatoithieu=giacong_epkim_dongiatoithieu,
            giacong_cangap_soduongchay=giacong_cangap_soduongchay,
            giacong_cangap_dongia=giacong_cangap_dongia,
            
            giacong_cangap_giatoithieu=giacong_cangap_giatoithieu,
            giacong_bekhuon_dongiakhuon=giacong_bekhuon_dongiakhuon,
            giacong_bekhuon_dongialuot=giacong_bekhuon_dongialuot
        )
        for key, value in solutions.items():
            solutions[key]['df'] = pd.DataFrame(value['df'])
        for key, value in solutions.items():
            return value['df'], value['data'], value['image'], value['image_2']

    if equipment_size == "Auto" and paper_size == "Custom":
        solutions = PaperCuttingSolution.solve_equipment_size_auto(
            paper_size=(custom_paper_width, custom_paper_height),
            piece_size=(int(piece_width), int(piece_height)),
            number_of_print_slide=num_of_slice,
            flip_type=flip_type,
            paper_type=type_paper,
            paper_gsm=paper_weight,
            piece_quantity=piece_quantity,
            num_of_color=num_of_color,
            giacong_canmang_dongia=giacong_canmang_dongia,
            giacong_canmang_matphut=giacong_canmang_matphut,
            giacong_epkim_chieudai=giacong_epkim_chieudai,
            giacong_epkim_chieurong=giacong_epkim_chieurong,
            giacong_epkim_dongiakhuon=giacong_epkim_dongiakhuon,
            giacong_epkim_dongialuot=giacong_epkim_dongialuot,
            giacong_epkim_dongiatoithieu=giacong_epkim_dongiatoithieu,
            giacong_cangap_soduongchay=giacong_cangap_soduongchay,
            giacong_cangap_dongia=giacong_cangap_dongia,
            
            giacong_cangap_giatoithieu=giacong_cangap_giatoithieu,
            giacong_bekhuon_dongiakhuon=giacong_bekhuon_dongiakhuon,
            giacong_bekhuon_dongialuot=giacong_bekhuon_dongialuot
        )
        for key, value in solutions.items():
            solutions[key]['df'] = pd.DataFrame(value['df'])
        for key, value in solutions.items():
            return value['df'], value['data'], value['image'], value['image_2']

    if equipment_size == "Auto" and paper_size not in ["Custom", "Auto"]:
        paper_size = list(map(int, paper_size.split('x')))
        solutions = PaperCuttingSolution.solve_equipment_size_auto(
            paper_size=(paper_size[0], paper_size[1]),
            piece_size=(int(piece_width), int(piece_height)),
            number_of_print_slide=num_of_slice,
            flip_type=flip_type,
            paper_type=type_paper,
            paper_gsm=paper_weight,
            piece_quantity=piece_quantity,
            num_of_color=num_of_color,
            giacong_canmang_dongia=giacong_canmang_dongia,
            giacong_canmang_matphut=giacong_canmang_matphut,
            giacong_epkim_chieudai=giacong_epkim_chieudai,
            giacong_epkim_chieurong=giacong_epkim_chieurong,
            giacong_epkim_dongiakhuon=giacong_epkim_dongiakhuon,
            giacong_epkim_dongialuot=giacong_epkim_dongialuot,
            giacong_epkim_dongiatoithieu=giacong_epkim_dongiatoithieu,
            giacong_cangap_soduongchay=giacong_cangap_soduongchay,
            giacong_cangap_giatoithieu=giacong_cangap_giatoithieu,
            giacong_cangap_dongia=giacong_cangap_dongia,
            
            giacong_bekhuon_dongiakhuon=giacong_bekhuon_dongiakhuon,
            giacong_bekhuon_dongialuot=giacong_bekhuon_dongialuot
        )
        for key, value in solutions.items():
            solutions[key]['df'] = pd.DataFrame(value['df'])
        for key, value in solutions.items():
            return value['df'], value['data'], value['image'], value['image_2']


def update_interface(equipment_size, paper_size):
    global solutions
    key = "Khổ máy: {} & Khổ giấy: {}".format(equipment_size, paper_size)
    solution = solutions.get(
        key, {"data": "Chưa có lời giải", "image": None, "df": None, "image_2": None})
    return solution["df"], solution["data"], solution["image"], solution["image_2"]


def update_interface_input(solution_data, equipment_size, paper_size):
    try:
        # Khổ máy: 790x1090 & Khổ giấy: 790x1090
        equipment_size = solution_data.split('&')[0].split(':')[1].strip()
        paper_size = solution_data.split('&')[1].split(':')[1].strip()

        # equipment_size, paper_size = solution_data.split('#')[0].split('-')
        return equipment_size, paper_size
    except BaseException:
        return equipment_size, paper_size


# with gr.Blocks(theme="Roboto") as demo:
with gr.Blocks(theme=gr.themes.Default(font=[gr.themes.GoogleFont("Roboto"), "Roboto", "Roboto"])) as demo:
    gr.Markdown("# Demo Hệ thống tính toán")
    with gr.Tab("Tính toán giá in"):
        with gr.Row():
            with gr.Column(scale=5):
                # Kích thước mảnh
                gr.Markdown("## Số liệu tính toán")
                with gr.Row():
                    piece_width = gr.Number(
                        label="Chiều ngang (A)",
                        info="Kích thước thành phẩm")
                    piece_height = gr.Number(
                        label="Chiều dọc (B)",
                        info="Kích thước thành phẩm")
                with gr.Row():
                    piece_quantity = gr.Number(
                        label="Số lượng in (Z)",
                        info="Số lượng thành phẩm cần in")
                    num_of_slice = gr.Dropdown(
                        label="Số mặt in (W)", choices=[
                            1, 2], info="Chọn số mặt in", value=1, interactive=True)
                    # Chiều lật # TODO: Chưa sử dụng
                    flip_type = gr.Dropdown(
                        label="Chiều lật",
                        choices=[
                            "Auto",
                            "Ngang",
                            "Dọc"],
                        info="Chiều lật",
                        value="Auto",
                        interactive=True,
                        visible=False)
                with gr.Row():
                    type_paper = gr.Dropdown(
                        label="Loại giấy (O)",
                        choices=list_type_papers,
                        info="Chọn loại giấy",
                        value=list_type_papers[0])
                    paper_weight = gr.Dropdown(
                        label="Định lượng giấy (E)",
                        choices=mapping_paper_weight.get(list_type_papers[0], []),
                        info="Chọn định lượng giấy",
                        value=80,
                        interactive=True)
                with gr.Row():
                    equipment_size = gr.Dropdown(
                        label="Khổ máy (X, Y)",
                        choices=list_equipment_size,
                        info="Kích thước khổ máy",
                        value="Auto",
                        interactive=True)
                # with gr.Row():
                    paper_size = gr.Dropdown(
                        label="Khổ giấy (U, V)",
                        choices=paper_size_list,
                        info="Kích thước khổ máy",
                        value="Auto",
                        interactive=True)
                with gr.Row():
                    custom_equipment_width = gr.Number(
                        label="Chiều ngang (X)", info="Khổ máy", visible=False)
                    custom_equipment_height = gr.Number(
                        label="Chiều dọc (Y)", info="Khổ máy", visible=False)
                # with gr.Row():
                    custom_paper_width = gr.Number(
                        label="Chiều ngang (U)", info="Khổ giấy", visible=False, value=0)
                    custom_paper_height = gr.Number(
                        label="Chiều dọc (V)", info="Khổ giấy", visible=False, value=0)
                with gr.Row():
                    num_of_color = gr.Number(
                        label="Số màu của khổ máy", info="Số màu", value=4
                    )
                with gr.Row():
                    btn = gr.Button("Tính toán chi phí in")

                # Gia công
                gr.Markdown("## Số liệu gia công")
                
                gr.Markdown("### Cán màng (H1)")
                with gr.Row():
                    giacong_canmang_dongia = gr.Number(label="Đơn giá (VNĐ)")
                    giacong_canmang_matphut = gr.Dropdown([1, 2], label="Mặt phủ", value=1)
                
                gr.Markdown("### Ép kim (H2)")
                with gr.Row():
                    giacong_epkim_chieudai = gr.Number(label="Chiều dài")
                    giacong_epkim_chieurong = gr.Number(label="Chiều rộng")
                with gr.Row():
                    giacong_epkim_dongiakhuon = gr.Number(label="Đơn giá khuôn (VNĐ)")
                    giacong_epkim_dongialuot = gr.Number(label="Đơn giá lượt (VNĐ)")
                    giacong_epkim_dongiatoithieu = gr.Number(label="Đơn giá tối thiểu (VNĐ)")
                
                gr.Markdown("### Cấn gấp, demi (H3)")
                with gr.Row():
                    giacong_cangap_soduongchay = gr.Number(label="Số đường chạy")
                    giacong_cangap_dongia = gr.Number(label="Đơn giá")
                    giacong_cangap_giatoithieu = gr.Number(label="Giá tối thiểu")
                
                gr.Markdown("### Bế khuôn (H4)")
                with gr.Row():
                    giacong_bekhuon_dongiakhuon = gr.Number(label="Đơn giá khuôn")
                    giacong_bekhuon_dongialuot = gr.Number(label="Đơn giá lượt")
                    
                    
                type_paper.change(
                    update_paper_weight,
                    inputs=[type_paper],
                    outputs=[paper_weight])
                equipment_size.change(
                    update_custom_equipment_size,
                    inputs=[equipment_size],
                    outputs=[
                        custom_equipment_width,
                        custom_equipment_height])
                paper_size.change(
                    update_custom_paper_size,
                    inputs=[paper_size],
                    outputs=[
                        custom_paper_width,
                        custom_paper_height])

                num_of_slice.change(
                    update_custom_num_of_slice,
                    inputs=[num_of_slice],
                    outputs=[flip_type])

            # Column kết quả
            with gr.Column(scale=6):
                gr.Markdown("## Kết quả")
                solution_data = gr.Textbox(
                    label="Thông tin", visible=True)
                solution_df = gr.Dataframe(
                    headers=["(Trường thông tin)", "Xếp tối ưu", "Xếp đều"],
                    datatype=["str", "str", "str"],
                    row_count=1,
                    col_count=(3, "fixed")
                )
                solution_img = gr.Image(label="Hình ảnh kết quả Xếp Tối ưu")
                solution_img_2 = gr.Image(label="Hình ảnh kết quả Xếp đều")
                
    with gr.Tab("Cấu hình"):
        with gr.Column():
            gr.Markdown("### Khổ máy")
            data = gr.Dataframe(
                headers=["Loại máy", "Chiều dọc", "Chiều ngang", "Số màu mặc định", "Số màu tối đa", "Giá máy", "Đơn giá phụ trội"],
                datatype=["str", "str", "str", "str", "str", "number", "number"],
                # row_count=10,
                col_count=(7, "fixed"),
                value=data_df,
                interactive = False
            )
            
    with gr.Tab("Diện tích hộp"):
        with gr.Row():
            with gr.Column():
                gr.Markdown('### Mẫu')
                _ = gr.Image(value="sample.jpg", interactive=True, width=500)
            with gr.Column():        
                gr.Markdown("Nhập kích thước tính toán")
                with gr.Row():
                    h = gr.Number(label="Kích thước `h` (mm)")
                    w = gr.Number(label="Kích thước `w` (mm)")
                with gr.Row():
                    G = gr.Number(label="Kích thước `G` (mm)")
                    D = gr.Number(label="Kích thước `D` (mm)")
            
                button_area = gr.Button("Tính toán")
                
                with gr.Row():
                    area_result = gr.Textbox(label="Kết quả diện tích")
                    with gr.Column():
                        w_result = gr.Textbox(label="Chiều rộng")
                        h_result = gr.Textbox(label="Chiều dài")
    
    with gr.Tab("Nét ảnh"):
        with gr.Row():
            with gr.Column():
                input_image = gr.Image(label="Ảnh đầu vào", width=500)
                scale_number = gr.Slider(label="Scale", minimum=1, maximum=4, value=2)
                button_upscale = gr.Button("Process")
            with gr.Column():
                output_image = gr.Image(label="Ảnh đầu ra", width=500)
                output_info = gr.Text(label="Thông tin")

    button_upscale.click(
        process_upscale,
        inputs=[input_image, scale_number],
        outputs=[output_image, output_info]
    )

    button_area.click(
        calculate_area, 
        inputs=[w, h, G, D],
        outputs=[area_result, w_result, h_result]
    )
               
    btn.click(
        calculate_solutions,
        inputs=[
            piece_width,
            piece_height,
            piece_quantity,
            num_of_slice,
            flip_type,
            type_paper,
            paper_weight,
            equipment_size,
            custom_equipment_width,
            custom_equipment_height,
            paper_size,
            custom_paper_width,
            custom_paper_height,
            num_of_color,
            giacong_canmang_dongia,
            giacong_canmang_matphut,
            giacong_epkim_chieudai,
            giacong_epkim_chieurong,
            giacong_epkim_dongiakhuon,
            giacong_epkim_dongialuot,
            giacong_epkim_dongiatoithieu,
            giacong_cangap_soduongchay,
            giacong_cangap_dongia,
            giacong_cangap_giatoithieu,
            giacong_bekhuon_dongiakhuon,
            giacong_bekhuon_dongialuot],
        outputs=[
            solution_df,
            solution_data,
            solution_img, solution_img_2])
    solution_data.change(
        update_interface_input,
        inputs=[solution_data, equipment_size, paper_size],
        outputs=[equipment_size, paper_size]
    )
    equipment_size.change(
        update_interface,
        inputs=[equipment_size, paper_size],
        outputs=[solution_df, solution_data, solution_img, solution_img_2]
    )
    paper_size.change(
        update_interface,
        inputs=[equipment_size, paper_size],
        outputs=[solution_df, solution_data, solution_img, solution_img_2]
    )

if __name__ == "__main__":
    demo.launch(server_name="0.0.0.0", server_port=7860)
