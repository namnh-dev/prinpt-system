import numpy as np
from PIL import Image
import json

import math
import matplotlib.pyplot as plt
import matplotlib.patches as patches

DATA_PAPER = json.load(open('data_paper.json'))
DATA_EQUIPMENT = json.load(open('data_equipment.json'))


def format_currency(value):
    return "{:,} VND".format(value)


class PaperCuttingSolution:
    @staticmethod
    def plot_combined_solution(info, best_solution, fn="best_solution.png"):
        plt.clf()
        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 6))
        # Hình vẽ cho cutted_piece_on_subpaper
        PaperCuttingSolution.plot_solution(
            ax1,
            best_solution['cutted_piece_on_subpaper'],
            best_solution['processed_piece_on_subpaper_size'][0],
            best_solution['processed_piece_on_subpaper_size'][1],
            'cutted_piece_on_subpaper')

        # Hình vẽ cho cutted_subpaper_on_paper
        PaperCuttingSolution.plot_solution(
            ax2,
            best_solution['cutted_subpaper_on_paper'],
            best_solution['processed_subpaper_on_paper'][0],
            best_solution['processed_subpaper_on_paper'][1],
            'cutted_subpaper_on_paper')
        # ax1.title(f'Kết quả xếp mảnh thành phẩm {info["piece_size"]} trên khổ cắt {info["processed_subpaper_on_paper"]}', loc='bottom')
        # ax2.set_title('Tiêu đề cho cutted_subpaper_on_paper')

        plt.tight_layout()
        plt.savefig(fn)

    @staticmethod
    def plot_solution(ax, cuts, W, H, solution_type):
        w_max = max(cuts, key=lambda x: x['w'] + x['x'])
        h_max = max(cuts, key=lambda x: x['h'] + x['y'])
        w_max = w_max['w'] + w_max['x']
        h_max = h_max['h'] + h_max['y']
        if w_max > h_max:
            if W < H:
                W, H = H, W 
        else:
            if W > H:
                W, H = H, W
                
        ax.add_patch(
            patches.Rectangle(
                (0, 0), W, H, edgecolor='black', facecolor='none'))
        
        
        # Đánh số từng box
        box_number = 1
        for i, cut in enumerate(cuts):
            color = "blue" if solution_type == 'cutted_piece_on_subpaper' else "green"
            rect = patches.Rectangle(
                (cut['x'], cut['y']),
                cut['w'],
                cut['h'],
                edgecolor=color,
                facecolor='none')
            ax.add_patch(rect)
            # Đặt số vào giữa mỗi box
            ax.text(
                cut['x'] + cut['w'] / 2,
                cut['y'] + cut['h'] / 2,
                str(box_number),
                ha='center',
                va='center')
            box_number += 1
        ax.set_xlim(0, W)
        ax.set_ylim(0, H)
        ax.axis('off')
        ax.set_aspect('equal')
        ax.text(0.5, -0.05, f'{W}', ha='center',
                va='center', transform=ax.transAxes)
        ax.text(-0.05,
                0.5,
                f'{H}',
                ha='center',
                va='center',
                transform=ax.transAxes,
                rotation='vertical')
        ax.invert_yaxis()

    @staticmethod
    def can_fit_in_box(piece_size, box_size):
        '''Kiểm tra một mảnh có thể bỏ vào 1 hộp hay không'''
        w, h = piece_size
        A, B = box_size
        return (w <= A and h <= B) or (w <= B and h <= A)

    @staticmethod
    def find_cut_dimensions(paper_size, num_piece):
        '''Cắt giấy thành N mảnh bằng nhau'''
        if num_piece == 1:
            return paper_size, num_piece
        elif num_piece == 2:
            return (paper_size[0] // 2, paper_size[1]), num_piece
        elif num_piece == 3:
            return (paper_size[0] // 3, paper_size[1]), num_piece
        elif num_piece == 4:
            return (paper_size[0] // 2,
                    paper_size[1] // 2), num_piece
        elif num_piece == 6:
            return (paper_size[0] // 2,
                    paper_size[1] // 3), num_piece

    @staticmethod
    def plot_complete_solution(cuts, W, H, fn):
        fig, ax = plt.subplots()
        ax.add_patch(
            patches.Rectangle(
                (0, 0), W, H, edgecolor='black', facecolor='none'))
        # Đánh số từng box
        box_number = 1
        for i, cut in enumerate(cuts):
            color = "blue"
            rect = patches.Rectangle(
                (cut['x'],
                 cut['y']),
                cut['w'],
                cut['h'],
                edgecolor=color,
                facecolor='none')
            ax.add_patch(rect)
            # Đặt số vào giữa mỗi box
            ax.text(
                cut['x'] + cut['w'] / 2,
                cut['y'] + cut['h'] / 2,
                str(box_number),
                ha='center',
                va='center')
            box_number += 1
        ax.set_xlim(0, W)
        ax.set_ylim(0, H)
        # Loại bỏ trục tọa độ
        ax.axis('off')
        ax.set_aspect('equal')
        # Thêm nhãn x, y
        ax.text(0.5, -0.05, f'{W}', ha='center',
                va='center', transform=ax.transAxes)
        ax.text(-0.05,
                0.5,
                f'{H}',
                ha='center',
                va='center',
                transform=ax.transAxes,
                rotation='vertical')
        plt.gca().invert_yaxis()
        plt.savefig(fn)

    @staticmethod
    def calculate_bounding_rectangle_for_list(rectangles):
        x_min = min(rect['x'] for rect in rectangles)
        y_min = min(rect['y'] for rect in rectangles)
        x_max = max(rect['x'] + rect['w'] for rect in rectangles)
        y_max = max(rect['y'] + rect['h'] for rect in rectangles)

        return (x_max - x_min, y_max - y_min)

    @staticmethod
    def find_min_remainder(N, M, a, b, algo="hard"):
        if algo == "hard":
            min_remainder = float('inf')
            best_x, best_y = 0, 0
            for x in range(N // a + 1):
                if x and M < b:
                    continue
                for y in range(N // b + 1):
                    if y and M < a:
                        continue
                    total = x * a + y * b
                    if total <= N:
                        remainder = N - total
                        if remainder < min_remainder:
                            min_remainder = remainder
                            best_x, best_y = x, y

            return best_x, best_y, min_remainder
        else:
            return N // a, 0, N - N // a * a

    @staticmethod
    def calculate_paper_cutting(
            W,
            H,
            w,
            h,
            number_of_print_slide,
            flip_type="auto",
            algo="hard"):
        size1 = (w, h)
        size2 = (h, w)

        def optimal_cuts(
                W,
                H,
                size1,
                size2,
                x_offset=0,
                y_offset=0,
                cuts=None,
                total_cuts=0,
                algo="hard"):

            if cuts is None:
                cuts = []
            if not (size1[0] <= W and size1[1] <= H):
                return cuts, total_cuts

            a, b, _ = PaperCuttingSolution.find_min_remainder(
                W, H, size1[0], size2[0], algo)

            if a > 0 and b > 0:
                total_cuts += 3
            else:
                total_cuts += 1

            stacks_size1, stacks_size2 = H // size1[1], H // size2[1]
            # if algo == "hard":
                # if algo == "normal":
                # stacks_size1, stacks_size2 = W // size1[1], W // size2[1]

            for i in range(a):
                for j in range(stacks_size1):
                    cuts.append(
                        {
                            'x': x_offset + i * size1[0],
                            'y': y_offset + j * size1[1],
                            'w': size1[0],
                            'h': size1[1],
                            'type': 1})
            for i in range(b):
                for j in range(stacks_size2):
                    cuts.append(
                        {
                            'x': x_offset + a * size1[0] + i * size2[0],
                            'y': y_offset + j * size2[1],
                            'w': size2[0],
                            'h': size2[1],
                            'type': 2})

            # total_cuts += max(1,
            #                   a - 2) * max(1,
            #                                stacks_size1 - 2) + max(1,
            #                                                        b - 2) * max(1,
            # stacks_size2 - 2)
            
            if algo == "hard":
                remainders = [
                    (0, stacks_size1 * size1[1], a * size1[0], H),
                    (a * size1[0], stacks_size2 * size2[1], W, H),
                    (a * size1[0] + b * size2[0], 0, W, stacks_size2 * size2[1])
                ]
                for x1, y1, x2, y2 in remainders:
                    new_cuts, new_total = optimal_cuts(
                        x2 - x1, y2 - y1, size1, size2, x1, y1, cuts, total_cuts)
                    cuts, total_cuts = new_cuts, new_total
                    
            return cuts, total_cuts


        # Giải 1 mặt
        if number_of_print_slide == 1:
            cuts_1, total_cuts_1 = optimal_cuts(W, H, size1, size2, algo=algo)
            cuts_2, total_cuts_2 = optimal_cuts(H, W, size1, size2, algo=algo)

            if len(cuts_1) > len(cuts_2):
                cuts, total_cuts = cuts_1, total_cuts_1
            elif len(cuts_1) == len(cuts_2):
                if total_cuts_1 < total_cuts_2:
                    cuts, total_cuts = cuts_1, total_cuts_1
                else:
                    cuts, total_cuts = cuts_2, total_cuts_2
                    (W, H) = (H, W)
            else:
                cuts, total_cuts = cuts_2, total_cuts_2
                (W, H) = (H, W)

            area_used = len(cuts) * size1[0] * size1[1] / (W * H)
            return cuts, (W, H)

        # Giải 2 mặt
        elif number_of_print_slide == 2:
            tmp_cuts_1_W, tmp_total_cuts_1_W = optimal_cuts(
                W // 2, H, size1, size2, algo=algo)
            tmp_cuts_2_W, tmp_total_cuts_2_W = optimal_cuts(
                H // 2, W, size1, size2, algo=algo)
            total_cuts_1_W = tmp_total_cuts_1_W * 2
            total_cuts_2_W = tmp_total_cuts_2_W * 2

            # Trường hợp chỉ được 1 mảnh
            if len(tmp_cuts_1_W) == 0 and len(tmp_cuts_2_W) == 0:
                tmp_cuts_1_W, tmp_total_cuts_1_W = optimal_cuts(
                W, H, size1, size2, algo=algo)
                tmp_cuts_2_W, tmp_total_cuts_2_W = optimal_cuts(
                    H, W, size1, size2, algo=algo)
                
                if len(tmp_cuts_1_W) > len(tmp_cuts_2_W):
                    return tmp_cuts_1_W, (W, H)
                else:
                    return tmp_cuts_2_W, (H, W)
                    
            # Lấy đối xứng theo W
            # Tính chiều rộng lớn nhất W
            if len(tmp_cuts_1_W):
                W1 = max(rect['x'] + rect['w'] for rect in tmp_cuts_1_W) * 2
                # Tạo danh sách đối xứng
                symmetric_cuts_1 = []
                for rect in tmp_cuts_1_W:
                    new_x = W1 - (rect['x'] + rect['w'])
                    symmetric_cuts_1.append(
                        {'x': new_x, 'y': rect['y'], 'w': rect['w'], 'h': rect['h'], 'type': rect['type']})
                cuts_1_W = tmp_cuts_1_W + symmetric_cuts_1
            else:
                cuts_1_W = tmp_cuts_1_W

            # Tính chiều rộng lớn nhất W
            if len(tmp_cuts_2_W):
                W2 = max(rect['x'] + rect['w'] for rect in tmp_cuts_2_W) * 2
                # Tạo danh sách đối xứng
                symmetric_cuts_2 = []
                for rect in tmp_cuts_2_W:
                    new_x = W2 - (rect['x'] + rect['w'])
                    symmetric_cuts_2.append(
                        {'x': new_x, 'y': rect['y'], 'w': rect['w'], 'h': rect['h'], 'type': rect['type']})
                cuts_2_W = tmp_cuts_2_W + symmetric_cuts_2
            else:
                cuts_2_W = tmp_cuts_2_W

            # Tính theo H
            tmp_cuts_1_H, tmp_total_cuts_1_H = optimal_cuts(
                W, H // 2, size1, size2, algo=algo)
            tmp_cuts_2_H, tmp_total_cuts_2_H = optimal_cuts(
                H, W // 2, size1, size2, algo=algo)
            total_cuts_1_H = tmp_total_cuts_1_H * 2
            total_cuts_2_H = tmp_total_cuts_2_H * 2

            # Lấy đối xứng theo W
            # Tính chiều rộng lớn nhất W
            if len(tmp_cuts_1_H):
                H1 = max(rect['y'] + rect['h'] for rect in tmp_cuts_1_H) * 2
                # Tạo danh sách đối xứng
                symmetric_cuts_H = []
                for rect in tmp_cuts_1_H:
                    new_y = H1 - (rect['y'] + rect['h'])
                    symmetric_cuts_H.append(
                        {'x': rect['x'], 'y': new_y, 'w': rect['w'], 'h': rect['h'], 'type': rect['type']})

                cuts_1_H = tmp_cuts_1_H + symmetric_cuts_H
            else:
                cuts_1_H = tmp_cuts_1_H

            # Tính chiều rộng lớn nhất W
            if len(tmp_cuts_2_H):
                H2 = max(rect['y'] + rect['h'] for rect in tmp_cuts_2_H) * 2
                # Tạo danh sách đối xứng
                symmetric_cuts_H = []
                for rect in tmp_cuts_2_H:
                    new_y = H2 - (rect['y'] + rect['h'])
                    symmetric_cuts_H.append(
                        {'x': rect['x'], 'y': new_y, 'w': rect['w'], 'h': rect['h'], 'type': rect['type']})

                cuts_2_H = tmp_cuts_2_H + symmetric_cuts_H
            else:
                cuts_2_H = tmp_cuts_2_H

            idx_best = 0
            value_best = 0

            if flip_type.lower() == "auto":
                cuts_list = [cuts_1_W, cuts_2_W, cuts_1_H, cuts_2_H]
                size_list = [(W, H), (H, W), (W, H), (H, W)]
            elif flip_type.lower() == "dọc":
                cuts_list = [cuts_1_W, cuts_2_W]
                size_list = [(W, H), (H, W)]
            elif flip_type.lower() == "ngang":
                cuts_list = [cuts_1_H, cuts_2_H]
                size_list = [(W, H), (H, W)]
                
            
            for idx, _cuts in enumerate(cuts_list):
                value = len(_cuts)
                if value > value_best:
                    idx_best = idx
                    value_best = value
                    
            return cuts_list[idx_best], size_list[idx]

            # cuts_1 = cuts_1_W
            # cuts_2 = cuts_2_W
            total_cuts_1 = total_cuts_1_W
            total_cuts_2 = total_cuts_2_W

            # cuts_1 = cuts_1_H
            # cuts_2 = cuts_2_H
            total_cuts_1 = total_cuts_1_H
            total_cuts_2 = total_cuts_2_H

            # if len(cuts_1) > len(cuts_2):
            #     cuts, total_cuts = cuts_1, total_cuts_1
            # elif len(cuts_1) == len(cuts_2):
            #     if total_cuts_1 < total_cuts_2:
            #         cuts, total_cuts = cuts_1, total_cuts_1
            #     else:
            #         cuts, total_cuts = cuts_2, total_cuts_2
            #         (W, H) = (H, W)
            # else:
            #     cuts, total_cuts = cuts_2, total_cuts_2
            #     (W, H) = (H, W)

            # area_used = len(cuts) * size1[0] * size1[1] / (W * H)
            # if number_of_print_slide == 2:
            #     if len(cuts) % 2 != 0:
            #         cuts.pop(-1)

            # return cuts, (W, H)

    def solution(
            piece_size,
            equipment_size,
            paper_size,
            number_of_print_slide,
            flip_type,
            piece_quantity,
            paper_type,
            paper_gsm,
            num_of_color,
            giacong_canmang_dongia,
            giacong_canmang_matphut,
            giacong_epkim_chieudai,
            giacong_epkim_chieurong,
            giacong_epkim_dongiakhuon,
            giacong_epkim_dongialuot,
            giacong_epkim_dongiatoithieu,
            giacong_cangap_soduongchay,
            giacong_cangap_dongia,
            giacong_cangap_giatoithieu,
            giacong_bekhuon_dongiakhuon,
            giacong_bekhuon_dongialuot,
            algo="hard"):
        '''
        piece_size (A, B): Kích thước mảnh
        equipment_size (X, Y): Kích thước khổ máy
        paper_size (U, V): Kích thước khổ giấy

        number_of_print_slide (W): Số mặt in
        flip_type: Lật theo chiều nào
        piece_quantity (Z): Số lượng giấy cần in

        paper_type (O): Loại giấy
        paper_gsm (E): Định lượng giấy
        '''

        # > Cắt khổ giấy thành các tờ nhỏ phù hợp
        # Duyệt qua từng case có thể
        subpaper_list = [equipment_size]
        count_subpaper_list = [1]
        for num_piece in [1, 2, 3, 4, 6]:
            # Chéo chiều rộng chiều cao
            for flag in [0, 1]:
                if flag:
                    paper_size = paper_size[::-1]
                subpaper_size, count_subpaper = PaperCuttingSolution.find_cut_dimensions(
                    paper_size, num_piece)
                if PaperCuttingSolution.can_fit_in_box(
                        subpaper_size,
                        equipment_size) and PaperCuttingSolution.can_fit_in_box(
                        piece_size,
                        subpaper_size):
                    if subpaper_size[0] < subpaper_size[1]:
                        subpaper_size = subpaper_size[::-1]
                    subpaper_list.append(
                        subpaper_size)
                    
        if not subpaper_list:
            subpaper_list = [piece_size]
        best_solution, best_num_of_paper_used, best_count_subpaper_on_paper = None, float(
            'inf'), float('inf')

        for subpaper_size in set(subpaper_list):

            cutted_piece_on_subpaper, processed_piece_on_subpaper_size = PaperCuttingSolution.calculate_paper_cutting(
                subpaper_size[0], subpaper_size[1], piece_size[0], piece_size[1], number_of_print_slide, flip_type, algo)
            
            if not len(cutted_piece_on_subpaper):
                continue
            # Tối ưu vùng mảnh
            optimzed_subpaper_size = PaperCuttingSolution.calculate_bounding_rectangle_for_list(
                cutted_piece_on_subpaper)
            
            # Xếp mảnh lên khổ giấy nhỏ sau khi tối ưu vùng
            cutted_subpaper_on_paper, processed_subpaper_on_paper = PaperCuttingSolution.calculate_paper_cutting(
                paper_size[0], paper_size[1], optimzed_subpaper_size[0], optimzed_subpaper_size[1], 1, flip_type)

            # Phiên bản bình thường
            normal_cutted_subpaper_on_paper, normal_processed_subpaper_on_paper = PaperCuttingSolution.calculate_paper_cutting(
                paper_size[0], paper_size[1], subpaper_size[0], subpaper_size[1], 1, flip_type)


            cutted_subpaper_on_paper = normal_cutted_subpaper_on_paper
            processed_subpaper_on_paper = normal_processed_subpaper_on_paper
            
            if len(normal_cutted_subpaper_on_paper) == len(cutted_subpaper_on_paper):
                cutted_subpaper_on_paper = normal_cutted_subpaper_on_paper
                optimzed_subpaper_size = processed_piece_on_subpaper_size
                
            # Số mảnh giấy cắt được từ khổ giấy
            count_subpaper_on_paper = len(cutted_subpaper_on_paper)
            # Số mảnh giấy xếp được trên khổ giấy cắt
            count_piece_on_subpaper = len(cutted_piece_on_subpaper)

            # Số tờ giấy cần thiết
            if count_subpaper_on_paper * count_piece_on_subpaper == 0:
                continue
            num_of_paper_used = math.ceil(piece_quantity // (
                count_subpaper_on_paper * count_piece_on_subpaper))

            number_of_prints = num_of_paper_used * \
                count_subpaper_on_paper * number_of_print_slide

            if num_of_paper_used <= best_num_of_paper_used:
                if count_subpaper_on_paper < best_count_subpaper_on_paper:
                    # Cập nhập lời giải
                    best_solution = dict(
                        processed_piece_on_subpaper_size=optimzed_subpaper_size,
                        processed_subpaper_on_paper=processed_subpaper_on_paper,
                        count_piece_on_subpaper=count_piece_on_subpaper,
                        count_subpaper_on_paper=count_subpaper_on_paper,
                        num_of_paper_used=num_of_paper_used,
                        num_of_paper_cropped =num_of_paper_used * count_subpaper_on_paper,
                        num_of_prinpts=number_of_prints,
                        cutted_subpaper_on_paper=cutted_subpaper_on_paper,
                        cutted_piece_on_subpaper=cutted_piece_on_subpaper)

                    best_num_of_paper_used = num_of_paper_used
                    best_count_subpaper_on_paper = count_subpaper_on_paper

        # info = (piece_size, equipment_size, paper_size, number_of_print_slide, piece_quantity, paper_type, paper_gsm, algo)

        if not best_solution:
            return None, None, None
        info = {
            "processed_subpaper_on_paper": best_solution['processed_subpaper_on_paper'],
            "paper_size": paper_size,
            "equipment_size": equipment_size,
            "piece_size": piece_size}
        
        cost_data = PaperCuttingSolution.get_cost_on_case(
            best_solution,
            best_solution['num_of_paper_used'],
            best_solution['num_of_prinpts'],
            paper_type,
            paper_gsm,
            equipment_size,
            paper_size,
            number_of_print_slide,
            num_of_color,
            giacong_canmang_dongia,
            giacong_canmang_matphut,
            giacong_epkim_chieudai,
            giacong_epkim_chieurong,
            giacong_epkim_dongiakhuon,
            giacong_epkim_dongialuot,
            giacong_epkim_dongiatoithieu,
            giacong_cangap_soduongchay,
            giacong_cangap_dongia,
            giacong_cangap_giatoithieu,
            giacong_bekhuon_dongiakhuon,
            giacong_bekhuon_dongialuot
        )

        return info, best_solution, cost_data

    @staticmethod
    def get_cost_on_case(
        best_solution,
        num_of_paper_used,
        num_of_prinpts,
        paper_type,
        paper_gsm,
        equipment_size,
        paper_size,
        number_of_print_slide,
        num_of_color,
        giacong_canmang_dongia,
        giacong_canmang_matphut,
        giacong_epkim_chieudai,
        giacong_epkim_chieurong,
        giacong_epkim_dongiakhuon,
        giacong_epkim_dongialuot,
        giacong_epkim_dongiatoithieu,
        giacong_cangap_soduongchay,
        giacong_cangap_dongia,
        giacong_cangap_giatoithieu,
        giacong_bekhuon_dongiakhuon,
        giacong_bekhuon_dongialuot
    ):
        # Giá giấy
        paper_cost = 0
        if paper_size[0] > paper_size[1]:
            paper_size = paper_size[::-1]
        key_paper = f"{paper_type} # {paper_size[0]} x {paper_size[1]}"
        paper_cost = DATA_PAPER.get(key_paper, {}).get(str(paper_gsm), 1000)
        paper_price = paper_cost
        paper_cost = paper_cost * num_of_paper_used  # * number_of_print_slide
        # Gía máy
        equipment_cost = 0
        if equipment_size[0] > equipment_size[1]:
            equipment_size = equipment_size[::-1]
        key_equipment = f"{equipment_size[0]} x {equipment_size[1]}"
        equipment_cost = DATA_EQUIPMENT.get(
            key_equipment, {}).get(
            "normal", 1000000)

        penalty_quipment_cost = DATA_EQUIPMENT.get(
            key_equipment, {}).get("penalty", 100)
        penalty_quipment_cost = max(
            0, num_of_prinpts - 1000) * penalty_quipment_cost * num_of_color


        # best_solution = dict(
        #     processed_piece_on_subpaper_size=optimzed_subpaper_size,
        #     processed_subpaper_on_paper=processed_subpaper_on_paper,
        #     count_piece_on_subpaper=count_piece_on_subpaper,
        #     count_subpaper_on_paper=count_subpaper_on_paper,
        #     num_of_paper_used=num_of_paper_used,
        #     num_of_paper_cropped =num_of_paper_used * count_subpaper_on_paper,
        #     num_of_prinpts=number_of_prints,
        #     cutted_subpaper_on_paper=cutted_subpaper_on_paper,
        #     cutted_piece_on_subpaper=cutted_piece_on_subpaper)
        # Phí gia công:
        # G7: Kích thước giấy được cắt
        cost_H1 = best_solution['processed_piece_on_subpaper_size'][0] * best_solution['processed_piece_on_subpaper_size'][1] \
            * giacong_canmang_dongia * giacong_canmang_matphut \
            * (best_solution['num_of_paper_cropped']  - best_solution['num_of_paper_used'])
        
        
        cost_H2 = giacong_epkim_chieudai * giacong_epkim_chieurong * giacong_epkim_dongialuot \
            * (best_solution['num_of_paper_cropped']  - best_solution['num_of_paper_used']) \
            + giacong_epkim_dongiakhuon
        cost_H2 = max(cost_H2, giacong_epkim_dongiatoithieu)
        
        cost_H3 = best_solution['num_of_paper_cropped'] * giacong_cangap_soduongchay * giacong_cangap_soduongchay
        cost_H3 = max(cost_H3, giacong_cangap_giatoithieu)
        
        cost_H4 = best_solution['processed_piece_on_subpaper_size'][0] * best_solution['processed_piece_on_subpaper_size'][1] \
            * giacong_bekhuon_dongiakhuon \
            + (best_solution['num_of_paper_cropped']  - best_solution['num_of_paper_used']) \
            * giacong_bekhuon_dongialuot
        
        giacong_cost = cost_H1 + cost_H2 + cost_H3 + cost_H4
        
        cost_data = {
            "paper_price": paper_price,
            "paper_cost": paper_cost,
            "equipment_cost": equipment_cost,
            "penalty_quipment_cost": penalty_quipment_cost,
            "giacong_cost": giacong_cost,
            "giacong_canmang_cost": cost_H1,
            "giacong_epkim_cost": cost_H2,
            "giacong_cangap_cost": cost_H3,
            "giacong_bekhuon_cost": cost_H4,
            "total_cost": paper_cost + equipment_cost + penalty_quipment_cost + giacong_cost,
        }

        return cost_data

    @staticmethod
    def solve_multi(
        piece_size,
        number_of_print_slide,
        flip_type,
        paper_type,
        paper_gsm,
        piece_quantity,
        num_of_color,
        giacong_canmang_dongia,
        giacong_canmang_matphut,
        giacong_epkim_chieudai,
        giacong_epkim_chieurong,
        giacong_epkim_dongiakhuon,
        giacong_epkim_dongialuot,
        giacong_epkim_dongiatoithieu,
        giacong_cangap_soduongchay,
        giacong_cangap_dongia,
        giacong_cangap_giatoithieu,
        giacong_bekhuon_dongiakhuon,
        giacong_bekhuon_dongialuot
    ):
        all_solutions = {}

        # Từng khổ máy
        for equipment_size in [
            (360, 520),
            (520, 720),
            (790, 1090),
        ]:

            if paper_type == "Couche Pindo":
                list_size = [(650, 860)]
            else: 
                list_size = [(650, 860), (790, 1090)]
            for paper_size in list_size:
                subsolution = {}
                for algo in [
                    "hard",
                    "normal",
                ]:
                    data_solution = PaperCuttingSolution.solution(
                        piece_size=piece_size,
                        equipment_size=equipment_size,
                        paper_size=paper_size,
                        number_of_print_slide=number_of_print_slide,
                        flip_type=flip_type,
                        piece_quantity=piece_quantity,
                        paper_type=paper_type,
                        paper_gsm=paper_gsm,
                        num_of_color=num_of_color,
                        giacong_canmang_dongia=giacong_canmang_dongia,
                        giacong_canmang_matphut=giacong_canmang_matphut,
                        giacong_epkim_chieudai=giacong_epkim_chieudai,
                        giacong_epkim_chieurong=giacong_epkim_chieurong,
                        giacong_epkim_dongiakhuon=giacong_epkim_dongiakhuon,
                        giacong_epkim_dongialuot=giacong_epkim_dongialuot,
                        giacong_epkim_dongiatoithieu=giacong_epkim_dongiatoithieu,
                        giacong_cangap_soduongchay=giacong_cangap_soduongchay,
                        giacong_cangap_dongia=giacong_cangap_dongia,
                        giacong_cangap_giatoithieu=giacong_cangap_giatoithieu,
                        giacong_bekhuon_dongiakhuon=giacong_bekhuon_dongiakhuon,
                        giacong_bekhuon_dongialuot=giacong_bekhuon_dongialuot,
                        algo=algo
                    )

                    # TMP
                    info, solution, cost_data = data_solution
                    fn = None
                    if solution:
                        fn = f"plots/{piece_size[0]}x{piece_size[1]}-{equipment_size[0]}x{equipment_size[1]}-{paper_size[0]}x{paper_size[1]}-{number_of_print_slide}-{piece_quantity}-{paper_type}-{paper_gsm}-{algo}.png"
                        PaperCuttingSolution.plot_combined_solution(
                            info, solution, fn=fn)

                    subsolution[algo] = list(data_solution) + [fn]

                
                ## Check 
                key_solution = f"Khổ máy: {equipment_size[0]}x{equipment_size[1]} & Khổ giấy: {paper_size[0]}x{paper_size[1]}"

                # key_solution = f"{equipment_size[0]}x{equipment_size[1]}-{paper_size[0]}x{paper_size[1]}"
                df = {
                    "(Trường thông tin)": [
                        "Tổng chi phí",
                        "G2 - Chi phí máy in",
                        "G3 - Chi phí giấy in",
                        "G4 - Chi phí in phụ trội",
                        "G5 - Giá giấy",
                        "G6 - Số lượng khổ giấy",
                        "G7 - Kích thước giấy được cắt",
                        "G8 - Số lượng giấy được cắt",
                        "G9 - Số lượt in",
                        "G10 - Số thành phẩm trên bản in",
                        "G11 - Chi phí Gia công Cán màng",
                        "G12 - Chi phí Gia công Ép kim",
                        "G13 - Chi phí Gia công Cấn gấp",
                        "G14 - Chi phí Gia công Bé khuôn"],
                    "Xếp tối ưu": [
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",                        
                        "-",
                       "-",
                        "-",                        
                        "-",
                        "-",
                        "-"],
                    "Xếp đều": [
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",                        
                        "-",
                        "-",
                       "-",
                        "-",                        
                        "-",
                        "-",
                        "-"]}

                # if subsolution["normal"][2]:
                #     if subsolution["hard"][1]["num_of_paper_used"] == subsolution["normal"][1]["num_of_paper_used"]:
                #         subsolution['normal'][1] = None
                #         subsolution['normal'][2] = None
                #         subsolution['normal'][3] = None

                if subsolution["normal"][2] and subsolution["hard"][2]:
                    if subsolution["hard"][1]["num_of_paper_used"] > subsolution["normal"][1]["num_of_paper_used"]:
                        subsolution['hard'][1] = subsolution['normal'][1]
                        subsolution['hard'][2] = subsolution['normal'][2]
                        subsolution['hard'][3] = subsolution['normal'][3]

                if subsolution["hard"][1]:
                    df["Xếp tối ưu"] = [
                        format_currency(
                            subsolution["hard"][2]["total_cost"]),
                        format_currency(
                            subsolution["hard"][2]["equipment_cost"]),
                        format_currency(
                            subsolution["hard"][2]["paper_cost"]),
                        format_currency(
                            subsolution["hard"][2]["penalty_quipment_cost"]),
                        format_currency(
                            subsolution["hard"][2]["paper_price"]),
                        subsolution["hard"][1]["num_of_paper_used"],
                        f"{subsolution['hard'][1]['processed_piece_on_subpaper_size'][0]}x{subsolution['hard'][1]['processed_piece_on_subpaper_size'][1]}",
                        subsolution["hard"][1]["num_of_paper_cropped"],
                        subsolution["hard"][1]["num_of_prinpts"],
                        subsolution["hard"][1]["count_piece_on_subpaper"],
                        format_currency(
                            subsolution["hard"][2]["giacong_canmang_cost"]),
                        format_currency(
                            subsolution["hard"][2]["giacong_epkim_cost"]),
                        format_currency(
                            subsolution["hard"][2]["giacong_cangap_cost"]),
                        format_currency(
                            subsolution["hard"][2]["giacong_bekhuon_cost"])]
                    
                if subsolution["normal"][1]:
                    df["Xếp đều"] = [
                        format_currency(
                            subsolution["normal"][2]["total_cost"]),
                        format_currency(
                            subsolution["normal"][2]["equipment_cost"]),
                        format_currency(
                            subsolution["normal"][2]["paper_cost"]),
                        format_currency(
                            subsolution["normal"][2]["penalty_quipment_cost"]),
                        format_currency(
                            subsolution["normal"][2]["paper_price"]),
                        subsolution["normal"][1]["num_of_paper_used"],
                        f"{subsolution['normal'][1]['processed_piece_on_subpaper_size'][0]}x{subsolution['normal'][1]['processed_piece_on_subpaper_size'][1]}",
                        subsolution["normal"][1]["num_of_paper_cropped"],
                        subsolution["normal"][1]["num_of_prinpts"],
                        subsolution["normal"][1]["count_piece_on_subpaper"],
                        format_currency(
                            subsolution["normal"][2]["giacong_canmang_cost"]),
                        format_currency(
                            subsolution["normal"][2]["giacong_epkim_cost"]),
                        format_currency(
                            subsolution["normal"][2]["giacong_cangap_cost"]),
                        format_currency(
                            subsolution["normal"][2]["giacong_bekhuon_cost"])]
                    
                    
                    
                all_solutions[key_solution] = df
                all_solutions[key_solution] = {
                    "data": f"{key_solution}",
                    "df": df,
                    "image": subsolution["hard"][3],
                    "image_2": subsolution["normal"][3],
                }

        sorted_all_solutions = {}
        for k in all_solutions:
            if all_solutions[k]['image'] is not None:
                sorted_all_solutions[k] = all_solutions[k]
        # for k in all_solutions:
        #     if k not in sorted_all_solutions:
        #         sorted_all_solutions[k] = all_solutions[k]
        

        sorted_all_solutions = sorted(all_solutions.items(), key=lambda x: float(x[1]['df']['Xếp tối ưu'][0].replace(',', '').replace("VND", '').strip()))
        return {key: value for key, value in sorted_all_solutions}

    @staticmethod
    def solve_single(
        piece_size,
        paper_size,
        equipment_size,
        number_of_print_slide,
        flip_type,
        paper_type,
        paper_gsm,
        piece_quantity,
        num_of_color,
        giacong_canmang_dongia,
        giacong_canmang_matphut,
        giacong_epkim_chieudai,
        giacong_epkim_chieurong,
        giacong_epkim_dongiakhuon,
        giacong_epkim_dongialuot,
        giacong_epkim_dongiatoithieu,
        giacong_cangap_soduongchay,
        giacong_cangap_dongia,
        giacong_cangap_giatoithieu,
        giacong_bekhuon_dongiakhuon,
        giacong_bekhuon_dongialuot
    ):
        all_solutions = {}
        subsolution = {}
        for algo in [
            "hard",
            "normal"
        ]:
            data_solution = PaperCuttingSolution.solution(
                piece_size=piece_size,
                equipment_size=equipment_size,
                paper_size=paper_size,
                number_of_print_slide=number_of_print_slide,
                flip_type=flip_type,
                piece_quantity=piece_quantity,
                paper_type=paper_type,
                paper_gsm=paper_gsm,
                num_of_color=num_of_color,
                giacong_canmang_dongia=giacong_canmang_dongia,
                giacong_canmang_matphut=giacong_canmang_matphut,
                giacong_epkim_chieudai=giacong_epkim_chieudai,
                giacong_epkim_chieurong=giacong_epkim_chieurong,
                giacong_epkim_dongiakhuon=giacong_epkim_dongiakhuon,
                giacong_epkim_dongialuot=giacong_epkim_dongialuot,
                giacong_epkim_dongiatoithieu=giacong_epkim_dongiatoithieu,
                giacong_cangap_soduongchay=giacong_cangap_soduongchay,
                giacong_cangap_dongia=giacong_cangap_dongia,
                giacong_cangap_giatoithieu=giacong_cangap_giatoithieu,
                giacong_bekhuon_dongiakhuon=giacong_bekhuon_dongiakhuon,
                giacong_bekhuon_dongialuot=giacong_bekhuon_dongialuot,
                algo=algo
            )

            info, solution, cost_data = data_solution
            fn = None
            if solution:
                fn = f"plots/{piece_size[0]}x{piece_size[1]}-{equipment_size[0]}x{equipment_size[1]}-{paper_size[0]}x{paper_size[1]}-{number_of_print_slide}-{piece_quantity}-{paper_type}-{paper_gsm}-{algo}.png"
                PaperCuttingSolution.plot_combined_solution(
                    info, solution, fn=fn)

            subsolution[algo] = list(data_solution) + [fn]

        key_solution = f"Khổ máy: {equipment_size[0]}x{equipment_size[1]} & Khổ giấy: {paper_size[0]}x{paper_size[1]}"

        df = {
            "(Trường thông tin)": [
                "Tổng chi phí",
                "G2 - Chi phí máy in",
                "G3 - Chi phí giấy in",
                "G4 - Chi phí in phụ trội",
                "G5 - Giá giấy",
                "G6 - Số lượng khổ giấy",
                "G7 - Kích thước giấy được cắt",
                "G8 - Số lượng giấy được cắt",
                "G9 - Số lượt in",
                        "G10 - Số thành phẩm trên bản in",
                        "G11 - Chi phí Gia công Cán màng",
                        "G12 - Chi phí Gia công Ép kim",
                        "G13 - Chi phí Gia công Cấn gấp",
                        "G14 - Chi phí Gia công Bé khuôn"],
            "Xếp tối ưu": [
                "-",
                "-",
                "-",
                "-",
                "-",
                "-",
                "-",
                "-",                        
                "-",
                "-",
                "-",
                "-",                        
                "-",
                "-"],
            "Xếp đều": [
                "-",
                "-",
                "-",
                "-",
                "-",
                "-",
                "-",                        
                "-",
                "-",
                "-",
                "-",
                "-",                        
                "-",
                "-"]}

        if subsolution["normal"][2] and subsolution["hard"][2]:
            if subsolution["hard"][1]["num_of_paper_used"] >= subsolution["normal"][1]["num_of_paper_used"]:
                subsolution['hard'][1] = subsolution['normal'][1]
                subsolution['hard'][2] = subsolution['normal'][2]
                subsolution['hard'][3] = subsolution['normal'][3]

        if subsolution["hard"][1]:
            df["Xếp tối ưu"] = [
                format_currency(
                    subsolution["hard"][2]["total_cost"]),
                format_currency(
                    subsolution["hard"][2]["equipment_cost"]),
                format_currency(
                    subsolution["hard"][2]["paper_cost"]),
                format_currency(
                    subsolution["hard"][2]["penalty_quipment_cost"]),
                format_currency(
                    subsolution["hard"][2]["paper_price"]),
                subsolution["hard"][1]["num_of_paper_used"],
                f"{subsolution['hard'][1]['processed_piece_on_subpaper_size'][0]}x{subsolution['hard'][1]['processed_piece_on_subpaper_size'][1]}",
                subsolution["hard"][1]["num_of_paper_cropped"],
                subsolution["hard"][1]["num_of_prinpts"],
                subsolution["hard"][1]["count_piece_on_subpaper"],
                        format_currency(
                            subsolution["hard"][2]["giacong_canmang_cost"]),
                        format_currency(
                            subsolution["hard"][2]["giacong_epkim_cost"]),
                        format_currency(
                            subsolution["hard"][2]["giacong_cangap_cost"]),
                        format_currency(
                            subsolution["hard"][2]["giacong_bekhuon_cost"])]
            
        if subsolution["normal"][1]:
            df["Xếp đều"] = [
                format_currency(
                    subsolution["normal"][2]["total_cost"]),
                format_currency(
                    subsolution["normal"][2]["equipment_cost"]),
                format_currency(
                    subsolution["normal"][2]["paper_cost"]),
                format_currency(
                    subsolution["normal"][2]["penalty_quipment_cost"]),
                format_currency(
                    subsolution["normal"][2]["paper_price"]),
                subsolution["normal"][1]["num_of_paper_used"],
                f"{subsolution['normal'][1]['processed_piece_on_subpaper_size'][0]}x{subsolution['normal'][1]['processed_piece_on_subpaper_size'][1]}",
                subsolution["normal"][1]["num_of_paper_cropped"],
                subsolution["normal"][1]["num_of_prinpts"],
                subsolution["normal"][1]["count_piece_on_subpaper"],
                        format_currency(
                            subsolution["normal"][2]["giacong_canmang_cost"]),
                        format_currency(
                            subsolution["normal"][2]["giacong_epkim_cost"]),
                        format_currency(
                            subsolution["normal"][2]["giacong_cangap_cost"]),
                        format_currency(
                            subsolution["normal"][2]["giacong_bekhuon_cost"])]
                
        all_solutions[key_solution] = df
        all_solutions[key_solution] = {
            "data": f"{key_solution}",
            "df": df,
            "image": subsolution["hard"][3],
            "image_2": subsolution["normal"][3],
        }
        
        sorted_all_solutions = {}
        for k in all_solutions:
            if all_solutions[k]['image'] is not None:
                sorted_all_solutions[k] = all_solutions[k]
        for k in all_solutions:
            if k not in sorted_all_solutions:
                sorted_all_solutions[k] = all_solutions[k]

        return sorted_all_solutions

    @staticmethod
    def solve_paper_size_auto(
        equipment_size,
        piece_size,
        number_of_print_slide,
        flip_type,
        paper_type,
        paper_gsm,
        piece_quantity,
        num_of_color,
        giacong_canmang_dongia,
        giacong_canmang_matphut,
        giacong_epkim_chieudai,
        giacong_epkim_chieurong,
        giacong_epkim_dongiakhuon,
        giacong_epkim_dongialuot,
        giacong_epkim_dongiatoithieu,
        giacong_cangap_soduongchay,
        giacong_cangap_dongia,
        giacong_cangap_giatoithieu,
        giacong_bekhuon_dongiakhuon,
        giacong_bekhuon_dongialuot
    ):
        all_solutions = {}

        # Từng khổ máy
        for equipment_size in [
            equipment_size
        ]:

            if paper_type == "Couche Pindo":
                list_size = [(650, 860)]
            else: 
                list_size = [(650, 860), (790, 1090)]
            for paper_size in list_size:
                subsolution = {}
                for algo in [
                    "hard",
                    "normal",
                ]:
                    data_solution = PaperCuttingSolution.solution(
                        piece_size=piece_size,
                        equipment_size=equipment_size,
                        paper_size=paper_size,
                        number_of_print_slide=number_of_print_slide,
                        flip_type=flip_type,
                        piece_quantity=piece_quantity,
                        paper_type=paper_type,
                        paper_gsm=paper_gsm,
                        num_of_color=num_of_color,
                        giacong_canmang_dongia=giacong_canmang_dongia,
                        giacong_canmang_matphut=giacong_canmang_matphut,
                        giacong_epkim_chieudai=giacong_epkim_chieudai,
                        giacong_epkim_chieurong=giacong_epkim_chieurong,
                        giacong_epkim_dongiakhuon=giacong_epkim_dongiakhuon,
                        giacong_epkim_dongialuot=giacong_epkim_dongialuot,
                        giacong_epkim_dongiatoithieu=giacong_epkim_dongiatoithieu,
                        giacong_cangap_soduongchay=giacong_cangap_soduongchay,
                        giacong_cangap_dongia=giacong_cangap_dongia,
                        giacong_cangap_giatoithieu=giacong_cangap_giatoithieu,
                        giacong_bekhuon_dongiakhuon=giacong_bekhuon_dongiakhuon,
                        giacong_bekhuon_dongialuot=giacong_bekhuon_dongialuot,
                        algo=algo
                    )

                    # TMP
                    info, solution, cost_data = data_solution
                    fn = None
                    if solution:
                        fn = f"plots/{piece_size[0]}x{piece_size[1]}-{equipment_size[0]}x{equipment_size[1]}-{paper_size[0]}x{paper_size[1]}-{number_of_print_slide}-{piece_quantity}-{paper_type}-{paper_gsm}-{algo}.png"
                        PaperCuttingSolution.plot_combined_solution(
                            info, solution, fn=fn)

                    subsolution[algo] = list(data_solution) + [fn]

                
                ## Check 
                key_solution = f"Khổ máy: {equipment_size[0]}x{equipment_size[1]} & Khổ giấy: {paper_size[0]}x{paper_size[1]}"

                # key_solution = f"{equipment_size[0]}x{equipment_size[1]}-{paper_size[0]}x{paper_size[1]}"
                df = {
                    "(Trường thông tin)": [
                        "Tổng chi phí",
                        "G2 - Chi phí máy in",
                        "G3 - Chi phí giấy in",
                        "G4 - Chi phí in phụ trội",
                        "G5 - Giá giấy",
                        "G6 - Số lượng khổ giấy",
                        "G7 - Kích thước giấy được cắt",
                        "G8 - Số lượng giấy được cắt",
                        "G9 - Số lượt in",
                        "G10 - Số thành phẩm trên bản in",
                        "G11 - Chi phí Gia công Cán màng",
                        "G12 - Chi phí Gia công Ép kim",
                        "G13 - Chi phí Gia công Cấn gấp",
                        "G14 - Chi phí Gia công Bé khuôn"],
                    "Xếp tối ưu": [
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",                        
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-"],
                    "Xếp đều": [
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",                        
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-"],}

                # if subsolution["normal"][2]:
                #     if subsolution["hard"][1]["num_of_paper_used"] == subsolution["normal"][1]["num_of_paper_used"]:
                #         subsolution['normal'][1] = None
                #         subsolution['normal'][2] = None
                #         subsolution['normal'][3] = None

                if subsolution["normal"][2] and subsolution["hard"][2]:
                    if subsolution["hard"][1]["num_of_paper_used"] >= subsolution["normal"][1]["num_of_paper_used"]:
                        subsolution['hard'][1] = subsolution['normal'][1]
                        subsolution['hard'][2] = subsolution['normal'][2]
                        subsolution['hard'][3] = subsolution['normal'][3]

                if subsolution["hard"][1]:
                    df["Xếp tối ưu"] = [
                        format_currency(
                            subsolution["hard"][2]["total_cost"]),
                        format_currency(
                            subsolution["hard"][2]["equipment_cost"]),
                        format_currency(
                            subsolution["hard"][2]["paper_cost"]),
                        format_currency(
                            subsolution["hard"][2]["penalty_quipment_cost"]),
                        format_currency(
                            subsolution["hard"][2]["paper_price"]),
                        subsolution["hard"][1]["num_of_paper_used"],
                        f"{subsolution['hard'][1]['processed_piece_on_subpaper_size'][0]}x{subsolution['hard'][1]['processed_piece_on_subpaper_size'][1]}",
                        subsolution["hard"][1]["num_of_paper_cropped"],
                        subsolution["hard"][1]["num_of_prinpts"],
                        subsolution["hard"][1]["count_piece_on_subpaper"],
                    
                        format_currency(
                            subsolution["hard"][2]["giacong_canmang_cost"]),
                        format_currency(
                            subsolution["hard"][2]["giacong_epkim_cost"]),
                        format_currency(
                            subsolution["hard"][2]["giacong_cangap_cost"]),
                        format_currency(
                            subsolution["hard"][2]["giacong_bekhuon_cost"])]
                        
                if subsolution["normal"][1]:
                    df["Xếp đều"] = [
                        format_currency(
                            subsolution["normal"][2]["total_cost"]),
                        format_currency(
                            subsolution["normal"][2]["equipment_cost"]),
                        format_currency(
                            subsolution["normal"][2]["paper_cost"]),
                        format_currency(
                            subsolution["normal"][2]["penalty_quipment_cost"]),
                        format_currency(
                            subsolution["normal"][2]["paper_price"]),
                        subsolution["normal"][1]["num_of_paper_used"],
                        f"{subsolution['normal'][1]['processed_piece_on_subpaper_size'][0]}x{subsolution['normal'][1]['processed_piece_on_subpaper_size'][1]}",
                        subsolution["normal"][1]["num_of_paper_cropped"],
                        subsolution["normal"][1]["num_of_prinpts"],
                        subsolution["normal"][1]["count_piece_on_subpaper"],
                        format_currency(
                            subsolution["normal"][2]["giacong_canmang_cost"]),
                        format_currency(
                            subsolution["normal"][2]["giacong_epkim_cost"]),
                        format_currency(
                            subsolution["normal"][2]["giacong_cangap_cost"]),
                        format_currency(
                            subsolution["normal"][2]["giacong_bekhuon_cost"])]
                    
                all_solutions[key_solution] = df
                all_solutions[key_solution] = {
                    "data": f"{key_solution}",
                    "df": df,
                    "image": subsolution["hard"][3],
                    "image_2": subsolution["normal"][3],
                }

        sorted_all_solutions = {}
        for k in all_solutions:
            if all_solutions[k]['image'] is not None:
                sorted_all_solutions[k] = all_solutions[k]
        # for k in all_solutions:
        #     if k not in sorted_all_solutions:
        #         sorted_all_solutions[k] = all_solutions[k]
        

        sorted_all_solutions = sorted(all_solutions.items(), key=lambda x: int(x[1]['df']['Xếp tối ưu'][0].replace(',', '').replace("VND", '').strip()))
        return {key: value for key, value in sorted_all_solutions}

    @staticmethod
    def solve_equipment_size_auto(
        paper_size,
        piece_size,
        number_of_print_slide,
        flip_type,
        paper_type,
        paper_gsm,
        piece_quantity,
        num_of_color,
        giacong_canmang_dongia,
        giacong_canmang_matphut,
        giacong_epkim_chieudai,
        giacong_epkim_chieurong,
        giacong_epkim_dongiakhuon,
        giacong_epkim_dongialuot,
        giacong_epkim_dongiatoithieu,
        giacong_cangap_soduongchay,
        giacong_cangap_dongia,
        giacong_cangap_giatoithieu,
        giacong_bekhuon_dongiakhuon,
        giacong_bekhuon_dongialuot
    ):
        all_solutions = {}

        # Từng khổ máy
        for equipment_size in [
            (360, 520),
            (520, 720),
            (790, 1090),
        ]:

            for paper_size in [paper_size]:
                subsolution = {}
                for algo in [
                    "hard",
                    "normal",
                ]:
                    data_solution = PaperCuttingSolution.solution(
                        piece_size=piece_size,
                        equipment_size=equipment_size,
                        paper_size=paper_size,
                        number_of_print_slide=number_of_print_slide,
                        flip_type=flip_type,
                        piece_quantity=piece_quantity,
                        paper_type=paper_type,
                        paper_gsm=paper_gsm,
                        num_of_color=num_of_color,
                        giacong_canmang_dongia=giacong_canmang_dongia,
                        giacong_canmang_matphut=giacong_canmang_matphut,
                        giacong_epkim_chieudai=giacong_epkim_chieudai,
                        giacong_epkim_chieurong=giacong_epkim_chieurong,
                        giacong_epkim_dongiakhuon=giacong_epkim_dongiakhuon,
                        giacong_epkim_dongialuot=giacong_epkim_dongialuot,
                        giacong_epkim_dongiatoithieu=giacong_epkim_dongiatoithieu,
                        giacong_cangap_soduongchay=giacong_cangap_soduongchay,
                        giacong_cangap_dongia=giacong_cangap_dongia,
                        giacong_cangap_giatoithieu=giacong_cangap_giatoithieu,
                        giacong_bekhuon_dongiakhuon=giacong_bekhuon_dongiakhuon,
                        giacong_bekhuon_dongialuot=giacong_bekhuon_dongialuot,
                        algo=algo
                    )

                    # TMP
                    info, solution, cost_data = data_solution
                    fn = None
                    if solution:
                        fn = f"plots/{piece_size[0]}x{piece_size[1]}-{equipment_size[0]}x{equipment_size[1]}-{paper_size[0]}x{paper_size[1]}-{number_of_print_slide}-{piece_quantity}-{paper_type}-{paper_gsm}-{algo}.png"
                        PaperCuttingSolution.plot_combined_solution(
                            info, solution, fn=fn)

                    subsolution[algo] = list(data_solution) + [fn]

                
                ## Check 
                key_solution = f"Khổ máy: {equipment_size[0]}x{equipment_size[1]} & Khổ giấy: {paper_size[0]}x{paper_size[1]}"

                # key_solution = f"{equipment_size[0]}x{equipment_size[1]}-{paper_size[0]}x{paper_size[1]}"
                df = {
                    "(Trường thông tin)": [
                        "Tổng chi phí",
                        "G2 - Chi phí máy in",
                        "G3 - Chi phí giấy in",
                        "G4 - Chi phí in phụ trội",
                        "G5 - Giá giấy",
                        "G6 - Số lượng khổ giấy",
                        "G7 - Kích thước giấy được cắt",
                        "G8 - Số lượng giấy được cắt",
                        "G9 - Số lượt in",
                        "G10 - Số thành phẩm trên bản in",
                        "G11 - Chi phí Gia công Cán màng",
                        "G12 - Chi phí Gia công Ép kim",
                        "G13 - Chi phí Gia công Cấn gấp",
                        "G14 - Chi phí Gia công Bé khuôn"],
                    "Xếp tối ưu": [
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",                        
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-"],
                    "Xếp đều": [
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",                        
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-"],}

                # if subsolution["normal"][2]:
                #     if subsolution["hard"][1]["num_of_paper_used"] == subsolution["normal"][1]["num_of_paper_used"]:
                #         subsolution['normal'][1] = None
                #         subsolution['normal'][2] = None
                #         subsolution['normal'][3] = None

                if subsolution["normal"][2] and subsolution["hard"][2]:
                    if subsolution["hard"][1]["num_of_paper_used"] >= subsolution["normal"][1]["num_of_paper_used"]:
                        subsolution['hard'][1] = subsolution['normal'][1]
                        subsolution['hard'][2] = subsolution['normal'][2]
                        subsolution['hard'][3] = subsolution['normal'][3]

                if subsolution["hard"][1]:
                    df["Xếp tối ưu"] = [
                        format_currency(
                            subsolution["hard"][2]["total_cost"]),
                        format_currency(
                            subsolution["hard"][2]["equipment_cost"]),
                        format_currency(
                            subsolution["hard"][2]["paper_cost"]),
                        format_currency(
                            subsolution["hard"][2]["penalty_quipment_cost"]),
                        format_currency(
                            subsolution["hard"][2]["paper_price"]),
                        subsolution["hard"][1]["num_of_paper_used"],
                        f"{subsolution['hard'][1]['processed_piece_on_subpaper_size'][0]}x{subsolution['hard'][1]['processed_piece_on_subpaper_size'][1]}",
                        subsolution["hard"][1]["num_of_paper_cropped"],
                        subsolution["hard"][1]["num_of_prinpts"],
                        subsolution["hard"][1]["count_piece_on_subpaper"],
                        format_currency(
                            subsolution["hard"][2]["giacong_canmang_cost"]),
                        format_currency(
                            subsolution["hard"][2]["giacong_epkim_cost"]),
                        format_currency(
                            subsolution["hard"][2]["giacong_cangap_cost"]),
                        format_currency(
                            subsolution["hard"][2]["giacong_bekhuon_cost"])]
                    
                if subsolution["normal"][1]:
                    df["Xếp đều"] = [
                        format_currency(
                            subsolution["normal"][2]["total_cost"]),
                        format_currency(
                            subsolution["normal"][2]["equipment_cost"]),
                        format_currency(
                            subsolution["normal"][2]["paper_cost"]),
                        format_currency(
                            subsolution["normal"][2]["penalty_quipment_cost"]),
                        format_currency(
                            subsolution["normal"][2]["paper_price"]),
                        subsolution["normal"][1]["num_of_paper_used"],
                        f"{subsolution['normal'][1]['processed_piece_on_subpaper_size'][0]}x{subsolution['normal'][1]['processed_piece_on_subpaper_size'][1]}",
                        subsolution["normal"][1]["num_of_paper_cropped"],
                        subsolution["normal"][1]["num_of_prinpts"],
                        subsolution["normal"][1]["count_piece_on_subpaper"],
                        format_currency(
                            subsolution["normal"][2]["giacong_canmang_cost"]),
                        format_currency(
                            subsolution["normal"][2]["giacong_epkim_cost"]),
                        format_currency(
                            subsolution["normal"][2]["giacong_cangap_cost"]),
                        format_currency(
                            subsolution["normal"][2]["giacong_bekhuon_cost"])]
                    
                    
                    
                all_solutions[key_solution] = df
                all_solutions[key_solution] = {
                    "data": f"{key_solution}",
                    "df": df,
                    "image": subsolution["hard"][3],
                    "image_2": subsolution["normal"][3],
                }

        sorted_all_solutions = {}
        for k in all_solutions:
            if all_solutions[k]['image'] is not None:
                sorted_all_solutions[k] = all_solutions[k]
        # for k in all_solutions:
        #     if k not in sorted_all_solutions:
        #         sorted_all_solutions[k] = all_solutions[k]
        

        sorted_all_solutions = sorted(all_solutions.items(), key=lambda x: int(x[1]['df']['Xếp tối ưu'][0].replace(',', '').replace("VND", '').strip()))
        return {key: value for key, value in sorted_all_solutions}



if __name__ == "__main__":
    # piece_size = (520, 320)  # Case này sẽ cắt khổ giấy thành 3 mảnh
    # piece_size = (45, 65)
    # piece_size = (45, 65)
    piece_size = (500, 700)
    # piece_size = (210, 297)
    

    # equipment_size = (360, 520)
    # paper_size = (650, 860)
    equipment_size = (520, 720)
    paper_size = (650, 860)

    number_of_print_slide = 1
    piece_quantity = 1000
    paper_type = 'Offset'
    paper_gsm = 100

    solution = PaperCuttingSolution.solve_single(
        piece_size,
        equipment_size,
        paper_size,
        number_of_print_slide,
        "auto",
        piece_quantity,
        paper_type,
        paper_gsm,
    )
    # json.dump(solution, open("solution.json", "w"))
    # PaperCuttingSolution.solve_multi(
    #     piece_size,
    #     number_of_print_slide,
    #     "auto",
    #     paper_type,
    #     paper_gsm,
    #     piece_quantity
    # )
